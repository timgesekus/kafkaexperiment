#!/bin/bash

# needed as PID 1 is not killable within a container
java -jar -Dconfig.override_with_env_vars=true kafka-example.jar "$@"
