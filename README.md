# Kafka experiment client

Start a producer or consumer, that sends or receives a defined amount of message. 

## Running

Running a consumer

```` bash
docker run topfhelm42/kafkaexperiment consumer
````

Running a producer

```` bash
docker run topfhelm42/kafkaexperiment producer
````

## Configuration

Default configuration in _/src/main/resources/application.conf_

```` hocon
topic = "test"
bootstrap-server = "localhost:9092"
producer {
    number-of-records = 1000000
    sleep-between-sends = 100
}

consumer {
    consumer-group = "name"
    consumer-group = "holly-hell"
    number-of-records = 1000000
    poll-timeout-millis = 20
}
````

Override configuration parameters by setting an environment variable like

````bash
docker run -e CONFIG_FORCE_consumer_consumer-group="hereafter" topfhelm42/kafkaexperiment consumer
````


