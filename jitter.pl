#!/usr/bin/perl


open (FILE, 'timestamps');

$last = 0;
$count = 0;
while (<FILE>) {
chomp;
my ($n,$s,$t)  =  split /;/ ;
if ($last == 0 ) {
  $last = $t;
  $jitter = 0;
} else {
  $jitter = $t - $last;
  $last = $t;
  $count++;
  $lat=$t-$s;
}
print "$n;$s;$t;$jitter;$lat\n"
}

close (FILE);

exit;
