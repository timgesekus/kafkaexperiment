FROM maven:3-jdk-9

WORKDIR /usr/src/app
COPY pom.xml .
COPY settings.xml /usr/share/maven/ref/
COPY . .
RUN mvn -B -e -C -T 1C  -s /usr/share/maven/ref/settings.xml package 

# package without maven
FROM openjdk
COPY --from=0 /usr/src/app/target/*.jar ./kafka-example.jar
COPY scripts/run.sh /usr/local/bin/run.sh
COPY scripts/kill.sh /usr/local/bin/kill.sh
ENTRYPOINT ["run.sh"]
