package kafkaExample;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Runner {

    public static void main(String[] args) throws InterruptedException {
        Runnable worker = null;

        if(args.length == 1 && ( args[0].equals("producer") || args[0].equals("consumer") )) {
            worker = new Worker(args[0], ConfigFactory.load());
        }
        else {
            System.out.println("Don't know how to do. Use either 'producer' or 'consumer' as argument.");
            System.exit(1);
        }

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.execute(worker);

        SpringApplication.run(Runner.class, args);

        executor.shutdown();
        while(!executor.isTerminated()) { Thread.sleep(10); }
        System.out.println("All threads terminated.");
    }

}
