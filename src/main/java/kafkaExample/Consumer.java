package kafkaExample;

import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class Consumer {

    static Logger logger = LoggerFactory.getLogger(Producer.class);

    public static void main(ConnectionConfig connectionConfig, kafkaExample.ConsumerConfig  consumerConfig) {
        logger.info("Starting consumer");
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, connectionConfig.bootstrapServer);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.IntegerDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerConfig.consumerGroup);
        KafkaConsumer<Integer,String> consumer = new KafkaConsumer<Integer, String>(props);
        consumer.subscribe(Arrays.asList(connectionConfig.topic));
        int counter = 0;
        long last = 0;
        long jitter = 0;
        long currentTime = 0;
        long delay = 0;
        Duration timeout = Duration.ofMillis(consumerConfig.pollTimeoutMillis);
        while (counter <= consumerConfig.numberOfRecords) {
			      ConsumerRecords<Integer, String> recs = consumer.poll(timeout);
            if (recs.count() == 0) {
            } else {
                for (ConsumerRecord<Integer, String> rec : recs) {
                  SharedData.CurrentPartition.set(rec.partition());
                	currentTime = System.currentTimeMillis();
                    if (last == 0 ) {
                    	last = currentTime;
                    } 
                	jitter = currentTime - last;
                	last = currentTime;
                	delay = currentTime - Long.valueOf(rec.value());
                	System.out.printf("%s;%d;%s;%d;%s;%d;%d;%d\n ", consumerConfig.consumerName, rec.partition(), rec.key(), counter, rec.value(), currentTime,jitter,delay );
                	counter++;
                }
            }
        }
        consumer.close();
    }
}
