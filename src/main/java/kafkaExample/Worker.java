package kafkaExample;

import com.typesafe.config.Config;

public class Worker implements Runnable {

  private String command;
  private Config config;

  public Worker(String command, Config config){
    this.command = command;
    this.config = config;
  }

  @Override
  public void run() {
    try {
      processCommand();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void processCommand() throws InterruptedException {
    System.out.println(Thread.currentThread().getName()+" Start. Command = "+command);
    ConnectionConfig connectionConfig = new ConnectionConfig(config);
    if (this.command.equals("producer")) {
      ProducerConfig producerConfig = new ProducerConfig(config);
      Producer.main(connectionConfig, producerConfig);
    } else if (this.command.equals("consumer")) {
      ConsumerConfig consumerConfig = new ConsumerConfig(config);
      Consumer.main(connectionConfig, consumerConfig);
    }  else {
      throw new IllegalArgumentException("Don't know how to do with this argument. Use either 'producer' or 'consumer': " + this.command);
    }
    System.out.println(Thread.currentThread().getName()+" End.");
  }

  @Override
  public String toString(){
    return this.command;
  }
}
