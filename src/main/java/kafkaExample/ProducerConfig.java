package kafkaExample;

import com.typesafe.config.Config;

class ProducerConfig {

    public final long sleepBetweenSends;
    public final long numberOfRecords;
    
    ProducerConfig (Config config) {
        sleepBetweenSends = config.getLong("producer.sleep-between-sends");
        numberOfRecords = config.getInt("producer.number-of-records");
    }
}