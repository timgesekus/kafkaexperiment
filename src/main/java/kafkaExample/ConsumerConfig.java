package kafkaExample;

import com.typesafe.config.Config;

class ConsumerConfig {
    public final String consumerName;
    public final String consumerGroup;
    public final long numberOfRecords;
    public final long pollTimeoutMillis;

    ConsumerConfig(Config config) {
        consumerName = config.getString("consumer.consumer-name");
        consumerGroup = config.getString("consumer.consumer-group");
        numberOfRecords = config.getLong("consumer.number-of-records");
        pollTimeoutMillis = config.getLong("consumer.poll-timeout-millis");
    }
}