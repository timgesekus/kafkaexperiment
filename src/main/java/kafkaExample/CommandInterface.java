package kafkaExample;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
class CommandInterface {

  AtomicInteger currentPartition;

  CommandInterface() {
    System.out.println("created");
    this.currentPartition = SharedData.CurrentPartition;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/currentPartition",
      produces = "text/plain"
  )
  String getCurrentPartition() {
    return currentPartition.toString() + "\n";
  }

  @RequestMapping(
      method = RequestMethod.POST,
      value = "/commands/{command}",
      produces = "text/plain",
      consumes = "text/plain"
  )
  String executeCommand(@PathVariable("command") String command) {
    System.out.println("executing: " + command);
    switch (command) {
      case "die":
        long pid = ProcessHandle.current().pid();
        try {
          String killCommand = "kill.sh " + String.valueOf(pid);
          try {
            System.out.println("Executing kill command: " + killCommand);
            Runtime.getRuntime().exec(killCommand);
          }
          catch (Exception e) {
            // if we reach this point there is no kill.sh and this means
            // we are not running in the container, thus we use normal kill command
            killCommand = "kill -9 " + String.valueOf(pid);
            System.out.println("Seems we are not running in a container. Executing kill command: " + killCommand);
            Runtime.getRuntime().exec(killCommand);
          }
        }
        catch (IOException e) {}
    }
    return "Unknown command: " + command + "\n";
  }

}
