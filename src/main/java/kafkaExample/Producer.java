package kafkaExample;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Producer {
	static Logger logger = LoggerFactory.getLogger(Producer.class);

    public static void main(ConnectionConfig connectionConfig, kafkaExample.ProducerConfig  producerConfig) throws InterruptedException {
    	logger.error("Creating producer");
       
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, connectionConfig.bootstrapServer);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        logger.error("Creating producer");
        KafkaProducer<Integer, String> producer = new KafkaProducer<Integer, String>(props);
        for (int i = 0; i < producerConfig.numberOfRecords; i++) {
            ProducerRecord<Integer, String> data;
            data = new ProducerRecord<Integer, String>(connectionConfig.topic, i, String.format("%d", System.currentTimeMillis()));
            producer.send(data);
            Thread.sleep(producerConfig.sleepBetweenSends);
        }
        producer.close();
    }
}
