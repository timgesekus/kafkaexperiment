package kafkaExample;

import com.typesafe.config.Config;

class ConnectionConfig {
    final public String topic;
    final public String bootstrapServer;

    ConnectionConfig(Config config) {
        topic = config.getString("topic");
        bootstrapServer = config.getString("bootstrap-server");
    }
}